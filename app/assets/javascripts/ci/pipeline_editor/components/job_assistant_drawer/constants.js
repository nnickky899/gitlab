import { s__ } from '~/locale';

export const DRAWER_CONTAINER_CLASS = '.content-wrapper';

export const i18n = {
  ADD_JOB: s__('JobAssistant|Add job'),
};
